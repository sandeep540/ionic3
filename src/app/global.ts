export class GlobalVars {

    static   auth :String = '';
    static   csrf :String = '';
    static   url :String = '';

  constructor(auth: String, csrf: String, url: String) {
    GlobalVars.auth = auth;
    GlobalVars.csrf = csrf;
    GlobalVars.url = url;
  }

  setMyGlobalVar(auth: String, csrf: String, url: String) {
    GlobalVars.auth = auth;
    GlobalVars.csrf = csrf;
    GlobalVars.url = url;
  }

  getMyGlobalVarAuth() {
    return GlobalVars.auth
  }

  getMyGlobalVarCSRF() {
    return GlobalVars.csrf;
  }

  getMyGlobalVarURL() {
    return GlobalVars.url;
  }

}
