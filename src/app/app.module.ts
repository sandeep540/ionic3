import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { Seasons } from '../pages/seasons/seasons';
import { Settings } from '../pages/settings/settings';
import { SearchProduct } from '../pages/search-product/search-product';
import { Login } from './../pages/login/login';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { AuthService } from './../providers/auth-service';
import { SeasonDetailsService } from './../providers/season-details-service';

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { KeyValuePipe } from './../pipes/key-value-pipe';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    Seasons,
    Settings,
    SearchProduct,
    Login,
    KeyValuePipe
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    FormsModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    Seasons,
    Settings,
    SearchProduct,
    Login
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
     AuthService, SeasonDetailsService
  ]
})
export class AppModule {}
