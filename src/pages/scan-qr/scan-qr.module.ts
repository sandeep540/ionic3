import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScanQR } from './scan-qr';

@NgModule({
  declarations: [
    ScanQR,
  ],
  imports: [
    IonicPageModule.forChild(ScanQR),
  ],
  exports: [
    ScanQR
  ]
})
export class ScanQRModule {}
