import { Component , OnInit} from '@angular/core';
import { NavController } from 'ionic-angular';
import { Seasons } from '../seasons/seasons';
import { SeasonService } from '../../providers/season-service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [SeasonService]
})
export class HomePage  implements OnInit {
  seasons:any[];
  Auth;
  url;

  constructor(public navCtrl: NavController, private seasonservice: SeasonService, private storage: Storage) {
    // this.seasons = [];
    // this.seasons.push({name: 'Fall 2014 Mens Footwear', id: 1});
    // this.seasons.push({name: 'Fall 2014 Womens Footwear', id: 2});
    // this.seasons.push({name: 'Fall 2015 Mens Apparel', id: 3});
    // this.seasons.push({name: 'Fall 2016 Mens Footwear', id: 4});
    // this.seasons.push({name: 'Fall 2018 Unisex Footwear', id: 5});

    this.init().then((values) => {seasonservice.getSeasonList(this.Auth, this.url)
      .subscribe(
        resBody => this.seasons = resBody.elements,
        error => console.error('Error: ' + error)),
        () => console.log('Completed retriving the Seasons!' ) });
  }


  showSeasonDetails (season: String) {
      // alert ('Success');
      this.navCtrl.push(Seasons, {season: season})
  }

  public init() {

          //  private getAuthHeaders() { return Observable.fromPromise(this.storage.get('Auth')); }
          let promiseList: Promise<any>[] = [];
          promiseList.push(
           this.storage.get('Auth').then((Auth) => {
                console.log('Retrived Auth is', Auth);
                this.Auth = Auth;

              } ));
          promiseList.push(
              this.storage.get('url').then((url) => {
                console.log('Retrived url is', url);
                this.url = url;
              } ));

          return Promise.all(promiseList);
}

ngOnInit() { }

}
