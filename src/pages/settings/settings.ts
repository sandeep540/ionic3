import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the Settings page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class Settings {

  worklist: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.worklist = [];
    this.worklist.push({name: 'Approve Sample 10892', id: 1});
    this.worklist.push({name: 'Approve Material Sample 101', id: 2});
    this.worklist.push({name: 'Review Product Spec P101', id: 3});
    this.worklist.push({name: 'Review Specification R999', id: 4});
    this.worklist.push({name: 'Approve Workflow 5603', id: 5});


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Settings');
  }

}
