import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, Loading, AlertController} from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {

  loading: Loading;
  loginCredentials = { user: '', password: '', url: ''};

  constructor(private nav: NavController, private auth: AuthService, private alertCtrl: AlertController,
    private loadingCtrl: LoadingController) {
      console.log(this.loginCredentials);
  }


  public login() {

      //if(this.loginCredentials.url != '' && this.loginCredentials.url.length === 0) {
      //if (typeof this.loginCredentials.url!='undefined' && this.loginCredentials.url) {
      if(this.loginCredentials.url.length <= 0) {
        alert('URL is empty');
      }
      else if (this.loginCredentials.user.length <= 0) {
        alert('username is empty');
      }
      else if (this.loginCredentials.password.length <= 0) {
        alert('password is empty');
      }
      else  {
        this.showLoading();
        this.auth.login(this.loginCredentials).subscribe(allowed => {
          if (allowed) {
            this.nav.setRoot(TabsPage);
          } else {
            this.showError("Access Denied");
          }
        },
          error => {
            this.showError(error);
          });
      }

}

showLoading() {
  this.loading = this.loadingCtrl.create({
    content: 'Please wait...',
    dismissOnPageChange: true
  });
  this.loading.present();
}

showError(text) {
  this.loading.dismiss();

  let alert = this.alertCtrl.create({
    title: 'Fail',
    subTitle: text,
    buttons: ['OK']
  });
  alert.present(prompt);
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

}
