import { Component  } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ProductService } from '../../providers/product-service';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the SearchProduct page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-search-product',
  templateUrl: 'search-product.html',
  providers: [ProductService]
})
export class SearchProduct {
    products:any[];
    product = { id: ''};

    searchQuery: string = '';
    items: string[];

    Auth;
    url;

  constructor(public navCtrl: NavController, public navParams: NavParams, private productservice: ProductService, private storage: Storage) {
    this.initializeItems();
  }

  initializeItems() {
    this.init().then((values) => {  this.productservice.getProductList(this.product.id, this.Auth, this.url)
        .subscribe(
          resBody => this.items = resBody.elements,
          error => console.error('Error: ' + error)),
          () => console.log('Completed retriving the Products!') });
    }


  getItems(ev: any) {
      // Reset items back to all of the items
      this.initializeItems();
      // set val to the value of the searchbar
      let val = ev.target.value;

      // if the value is an empty string don't filter the items
      if (val && val.trim() != '') {
        this.items = this.items.filter((item) => {
          return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchProduct');
  }

  public init() {
      //  private getAuthHeaders() { return Observable.fromPromise(this.storage.get('Auth')); }
      let promiseList: Promise<any>[] = [];
      promiseList.push(
       this.storage.get('Auth').then((Auth) => {
            console.log('Retrived Auth is', Auth);
            this.Auth = Auth;

          } ));
      promiseList.push(
          this.storage.get('url').then((url) => {
            console.log('Retrived url is', url);
            this.url = url;
          } ));

      return Promise.all(promiseList);
}

}
