import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SeasonDetailsService } from '../../providers/season-details-service';

/**
 * Generated class for the Seasons page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-seasons',
  templateUrl: 'seasons.html',
})

export class Seasons {

//  seasonDetails: any[];

  seasonDetails = {
  'Name': '2014 Spring Mens Footwear',
  'Brand': 'Big Bazar',
  'Season': 'Spring',
}

  season: any;
  Auth;
  url;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage, private sdetails: SeasonDetailsService) {

    // this.seasonDetails = [];
    // this.seasonDetails.push({name: '2014 Spring Mens Footwear', id: 1, brand:'Big Bazar', season:'Spring', year: 2014, createdBy:'PLM Admin'});
    // this.seasonDetails.push({name: '2015 Spring Mens Footwear', id: 2, brand:'Big Bazar', season:'Spring', year: 2015, createdBy:'PLM Admin'});
    // this.seasonDetails.push({name: '2016 Spring Mens Footwear', id: 3, brand:'Big Bazar', season:'Spring', year: 2016, createdBy:'PLM Admin'});
    // this.seasonDetails.push({name: '2017 Spring Mens Footwear', id: 4, brand:'Big Bazar', season:'Spring', year: 2017, createdBy:'PLM Admin'});
    // this.seasonDetails.push({name: '2018 Spring Mens Footwear', id: 5, brand:'Big Bazar', season:'Spring', year: 2018, createdBy:'PLM Admin'});
    // this.seasonDetails.push({name: '2019 Spring Mens Footwear', id: 6, brand:'Big Bazar', season:'Spring', year: 2019, createdBy:'PLM Admin'});

    this.season = navParams.get('season');
    //let url = storage.get('url');
    //console.log(url);

    let vrId = this.season['LCSSEASON.BRANCHIDITERATIONINFO'];
    vrId = 'VR:com.lcs.wc.season.LCSSeason:' + vrId;

    this.init().then((values) => {  this.sdetails.getSeasonDetails(vrId, this.Auth, this.url)
        .subscribe(
          resBody => this.seasonDetails = resBody.attributes,
          error => console.error('Error: ' + error)),
          () => console.log('Completed retriving the Season Details!') });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Seasons');
  }


  public init() {
          //  private getAuthHeaders() { return Observable.fromPromise(this.storage.get('Auth')); }
          let promiseList: Promise<any>[] = [];
          promiseList.push(
           this.storage.get('Auth').then((Auth) => {
                console.log('Retrived Auth is', Auth);
                this.Auth = Auth;

              } ));
          promiseList.push(
              this.storage.get('url').then((url) => {
                console.log('Retrived url is', url);
                this.url = url;
              } ));

          return Promise.all(promiseList);
}

}
