import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import {Headers} from '@angular/http';
import { Storage } from '@ionic/storage';

/*
  Generated class for the SeasonDetailsService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class SeasonDetailsService {

  constructor(public http: Http, private storage: Storage) {
    console.log('Hello SeasonDetailsService Provider');
  }

  public getSeasonDetails(criteria: string, Auth:string, url: string) {

    let re = ':';
    let seasonValue = criteria.replace(re, "%3A");

    console.log('Encoded value -->'+seasonValue);
    const headers: Headers = new Headers();
    headers.append('Authorization', 'Basic ' + Auth);
    headers.append('Content-Type', 'application/json');
    return (this.http.get('http://' + url +'/Windchill/servlet/rest/rfa/instances/'+seasonValue,
    {headers: headers}).
    map((response: Response) =>  response.json()));
}


}
